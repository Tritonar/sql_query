/* crt_transito	--> create database smulta*/
create database if NOT EXISTS smulta;
use smulta;
 
CREATE TABLE if NOT EXISTS auto ( 
idDominio varchar (8),
marca varchar (10) ,
anio int,
chasis  varchar (20),
motor varchar(20),
dni int,
primary key (idDominio) 
); 
CREATE TABLE if NOT EXISTS licencia ( 
licNro int,
apellido varchar (20) ,
nombre varchar (20) ,
tipoLicencia ENUM ('Part','Prof','Tran','Pasa') default 'Part' not null,
puntos int,
dni int,
primary key (licNro) 
);

CREATE TABLE if NOT EXISTS tipoInf ( 
tipoI int ,
infDesc varchar (30) ,
infPuntos int,
primary key (tipoI) 
);
CREATE TABLE if NOT EXISTS infraccion ( 
idDominio varchar(8) ,
tipoI int,
licNro int,
infFechaHora datetime,
infDonde varchar(40),
primary key (infFechaHora), 
foreign key (idDominio) references auto (idDominio),
foreign key (licNro) references licencia (licNro),
foreign key (tipoI) references tipoInf (tipoI)
);

INSERT INTO tipoInf (tipoI, infDesc, infPuntos) VALUE (1,'SEMAFORO EN ROJO',20);
INSERT INTO tipoInf (tipoI, infDesc, infPuntos) VALUE (2,'MAL ESTACIONADO',10);
INSERT INTO tipoInf (tipoI, infDesc, infPuntos) VALUE (3,'EXCESO DE VELOCIDAD',30);
INSERT INTO tipoInf (tipoI, infDesc, infPuntos) VALUE (4,'SIN CINTURON',12);

INSERT INTO licencia (licNro,apellido,nombre,tipoLicencia,puntos,dni) VALUE (10677563,'PEREZ','MARIA','Part',100,10677563);
INSERT INTO licencia (licNro,apellido,nombre,tipoLicencia,puntos,dni) VALUE (11233445,'PALURDI','JUAN','Pasa',100,11233445);
INSERT INTO licencia (licNro,apellido,nombre,tipoLicencia,puntos,dni) VALUE (17111111,'ARANGUNDE','MARIAM','Prof',100,17111111);
INSERT INTO licencia (licNro,apellido,nombre,tipoLicencia,puntos,dni) VALUE (18564474,'AMARILLA','FERNANDO','Tran',100,18564474);
INSERT INTO licencia (licNro,apellido,nombre,tipoLicencia,puntos,dni) VALUE (21564444,'PEREZ','JUAN','Part',100,21564444);
INSERT INTO licencia (licNro,apellido,nombre,tipoLicencia,puntos,dni) VALUE (23545333,'PALURDI','HECTOR','Part',100,23545333);

INSERT INTO auto (idDominio,marca,anio,chasis,motor,dni) VALUE ('ABB122','PEUGEOT 205',1998,'DFJHFDJD828','CVMNX3482',11233445);
INSERT INTO auto (idDominio,marca,anio,chasis,motor,dni) VALUE ('MBN274','FIAT PUNTO',2013,'DGHDA43594','WOIQWIOF584',10677563);
INSERT INTO auto (idDominio,marca,anio,chasis,motor,dni) VALUE ('MNA163','FORD FIESTA',2013,'EJEWP44690','VVDKAK345',18564474);

INSERT INTO infraccion (idDominio, tipoI,licNro,infFechaHora,infDonde) VALUE ('ABB122',2,11233445,'2013-12-12 10:45:00','DFDJFD DJIH');
INSERT INTO infraccion (idDominio, tipoI,licNro,infFechaHora,infDonde) VALUE ('ABB122',3,NULL,'2014-01-01 23:12:00','XCMNXCN');
INSERT INTO infraccion (idDominio, tipoI,licNro,infFechaHora,infDonde) VALUE ('ABB122',1,NULL,'2014-03-12 09:45:00','VCMVMXMXM');
INSERT INTO infraccion (idDominio, tipoI,licNro,infFechaHora,infDonde) VALUE ('ABB122',1,11233445,'2014-04-23 17:22:00','AAJDAKJA');
INSERT INTO infraccion (idDominio, tipoI,licNro,infFechaHora,infDonde) VALUE ('ABB122',3,23545333,'2014-05-11 11:22:00','VCVCV,CVC');
INSERT INTO infraccion (idDominio, tipoI,licNro,infFechaHora,infDonde) VALUE ('MNA163',1,17111111,'2014-01-12 23:00:00','CVBCVCBXX');
INSERT INTO infraccion (idDominio, tipoI,licNro,infFechaHora,infDonde) VALUE ('MNA163',3,17111111,'2014-05-04 11:22:00','CXCMXCXM');
INSERT INTO infraccion (idDominio, tipoI,licNro,infFechaHora,infDonde) VALUE ('MNA163',2,17111111,'2014-06-01 23:12:00','QOHDHGC');
INSERT INTO infraccion (idDominio, tipoI,licNro,infFechaHora,infDonde) VALUE ('ABB122',2,11233445,'2013-11-11 11:11:11','DFDJFD DJIH');



/*1) Conductores que no tienen infracciones.*/
select * from  licencia  where not EXISTS 
(select licNro from infraccion where licencia.licNro=infraccion.licNro group by licNro );
/*
+----------+----------+----------+--------------+--------+----------+
| licNro   | apellido | nombre   | tipoLicencia | puntos | dni      |
+----------+----------+----------+--------------+--------+----------+
| 10677563 | PEREZ    | MARIA    | Part         |    100 | 10677563 |
| 18564474 | AMARILLA | FERNANDO | Tran         |    100 | 18564474 |
| 21564444 | PEREZ    | JUAN     | Part         |    100 | 21564444 |
+----------+----------+----------+--------------+--------++----------+
*/
/*2) Mayor cantidad de infracciones*/
select a.licNro, a.apellido, a.nombre, a.dni, tp. Multas  
from licencia a,(select licNro, count(*) Multas
	 from ((select idDominio, licNro, tipoI from infraccion where licNro is not null) 
 			union all (select ty.idDominio, auto.dni licnro, ty.tipoI   
 			from (select * from infraccion where licNro is null) ty, auto 
 			where ty.idDominio= auto.idDominio    ) ) as rc, tipoInf rg 
 	 where rg.tipoI = rc.tipoI group by licNro limit 1 ) tp 
where tp.licNro= a.licNro;

select infraccion.licNro,nombre,apellido, count(infraccion.licNro) multas 
from infraccion, licencia where  infraccion.licNro!=0  and infraccion.licNro=licencia.licNro 
group by infraccion.licNro 
having multas=(select fg.DER 
			   from (select licNro, count(*) DER from infraccion 
			   	group by licNro order by DER desc limit 1) fg);

/*
+----------+----------+--------+----------+--------+
| licNro   | apellido | nombre | dni      | Multas |
+----------+----------+--------+----------+--------+
| 11233445 | PALURDI  | JUAN   | 11233445 |      4 |
+----------+----------+--------+----------+--------+
Por cantidad de infracciones de menor a mayor*/
select a.licNro, a.apellido, a.nombre, a.dni, tp. Multas  
from licencia a,(select licNro, count(*) Multas 
	from (  (select idDominio, licNro, tipoI from infraccion where licNro is not null) 
		    union all 
		    (select ty.idDominio, auto.dni licnro, ty.tipoI   
			 from (select * from infraccion where licNro is null) ty, auto 
			 where ty.idDominio= auto.idDominio    ) )
	as rc, tipoInf rg 
	where rg.tipoI = rc.tipoI group by licNro ) tp 
where tp.licNro= a.licNro order by tp.Multas;

/*
+----------+-----------+--------+----------+--------+
| licNro   | apellido  | nombre | dni      | Multas |
+----------+-----------+--------+----------+--------+
| 23545333 | PALURDI   | HECTOR | 23545333 |      1 |
| 17111111 | ARANGUNDE | MARIAM | 17111111 |      3 |
| 11233445 | PALURDI   | JUAN   | 11233445 |      4 |
+----------+-----------+--------+----------+--------+
*/
/* 4) Actualice la tabla licencia restando los puntos*/
select a.licNro, a.apellido, a.nombre, a.dni, tp. Puntos  from licencia a,
	(select licNro, sum(rg.infPuntos) Puntos 
	 from ((select idDominio, licNro, tipoI from infraccion where licNro is not null) 
			union all (select ty.idDominio, auto.dni licnro, ty.tipoI   
						from (select * from infraccion where licNro is null) ty, auto 
						where ty.idDominio= auto.idDominio    ) ) as rc, tipoInf rg 
	 where rg.tipoI = rc.tipoI group by licNro desc ) tp
where tp.licNro= a.licNro;
/*
+----------+-----------+--------+----------+--------+
| licNro   | apellido  | nombre | dni      | Puntos |
+----------+-----------+--------+----------+--------+
| 11233445 | PALURDI   | JUAN   | 11233445 |     80 |
| 17111111 | ARANGUNDE | MARIAM | 17111111 |     60 |
| 23545333 | PALURDI   | HECTOR | 23545333 |     30 |
+----------+-----------+--------+----------+--------+*/
/* 4) Actualice la tabla licencia restando los puntos*/
select a.licNro, a.apellido, a.nombre, a.dni, (a.puntos -tp. Puntos) 'Puntos Tot' 
from licencia a,(select licNro, sum(rg.infPuntos) Puntos 
			     from ((select idDominio, licNro, tipoI from infraccion where licNro is not null) 
						union all (select ty.idDominio, auto.dni licnro, ty.tipoI   
								   from (select * from infraccion where licNro is null) ty, auto 
				 				   where ty.idDominio= auto.idDominio    ) ) as rc, tipoInf rg
				 where rg.tipoI = rc.tipoI group by licNro desc ) tp 
where tp.licNro= a.licNro; 
/*
+----------+-----------+--------+----------+------------+
| licNro   | apellido  | nombre | dni      | Puntos Tot |
+----------+-----------+--------+----------+------------+
| 11233445 | PALURDI   | JUAN   | 11233445 |         20 |
| 17111111 | ARANGUNDE | MARIAM | 17111111 |         40 |
| 23545333 | PALURDI   | HECTOR | 23545333 |         70 |
+----------+-----------+--------+----------+------------+ */



/* El dominio con mayor cantidad de infracciones*/
select idDominio, count(*) Multas from infraccion group by idDominio order by Multas desc limit 1 ;

/*
+-----------+--------+
| idDominio | Multas |
+-----------+--------+
| ABB122    |      5 |
+-----------+--------+
*/


select licNro , count(*) as cantidad 
from infraccion
where licNro>0
group by licNro
having count(*)=(select  count(licNro) from infraccion 
				  group by licNro order by count(licNro) desc limit 1)
;
/*order by count(*) desc;

+----------+----------+
| licNro   | cantidad |
+----------+----------+
| 17111111 |        3 |
+----------+----------+
*/