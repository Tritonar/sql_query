create database if NOT EXISTS banco;
use banco;


CREATE TABLE if NOT EXISTS tTipo ( 
tCod INT ,
tNombre varchar (20) ,
tMonto INT,
tDetalle varchar(50),
primary key (tCod) 
);

INSERT INTO tTipo (tCod,tNombre,tMonto,tDetalle) VALUE (131313,'LATA',0,'OTORGADA A LOS QUE COBRAN MENOS DE $4999');
INSERT INTO tTipo (tCod,tNombre,tMonto,tDetalle) VALUE (121212,'PLATEADA',4999,'OTORGADA A LOS QUE COBRAN MENOS DE $4999');
INSERT INTO tTipo (tCod,tNombre,tMonto,tDetalle) VALUE (101010,'DORADA',10000,'OTORGADA A LOS QUE COBRAN MENOS DE $10000');

CREATE TABLE if NOT EXISTS  pagoSueldo ( 
pagoFecha datetime default current_timestamp,
cuentaNro INT,
pagoMonto INT,
pagoDetalle varchar(50),
primary key (pagoFecha),
index(cuentaNro),
foreign key (cuentaNro) references persona(cuentaNro)
);

INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2017-01-03 12:00:11',14560312,4480,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2017-01-03 12:00:12',14560148,4580,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2017-01-03 12:00:13',14560147,4680,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2017-01-03 12:00:14',14560145,4780,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2017-01-03 12:00:15',14560121,4880,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2017-01-03 12:00:16',14560046,4980,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2017-01-03 12:00:17',14560045,5080,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2017-01-03 12:00:18',14560034,5180,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2017-01-03 12:00:19',14560021,5280,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2017-01-03 12:00:20',13260145,5380,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2017-01-03 12:00:21',11760312,5480,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2017-01-03 12:00:22',11366147,5580,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2017-01-04 12:00:23',11360148,6580,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2017-01-04 12:00:24',11360146,6680,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2017-01-04 12:00:25',10060367,6780,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2017-01-05 12:00:26',10060197,6880,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2017-01-05 12:00:27',10060187,6980,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2017-01-05 12:00:28',10060113,9780,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2017-01-05 12:00:29',10060123,9880,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2017-01-05 12:00:30',10060133,9980,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2017-01-06 12:00:31',10060143,10080,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2017-01-06 12:00:32',10060151,11080,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2017-01-06 12:00:33',10060161,12080,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2017-01-06 12:00:34',10060177,18080,' El sueldo fue depositado en el banco Violoncello');

/*   */

INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2016-12-03 12:00:11',14560312,4480,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2016-12-03 12:00:12',14560148,4580,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2016-12-03 12:00:13',14560147,4680,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2016-12-03 12:00:14',14560145,4780,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2016-12-03 12:00:15',14560121,4880,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2016-12-03 12:00:16',14560046,4980,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2016-12-03 12:00:17',14560045,5080,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2016-12-03 12:00:18',14560034,5180,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2016-12-03 12:00:19',14560021,5280,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2016-12-03 12:00:20',13260145,5380,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2016-12-03 12:00:21',11760312,5480,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2016-12-03 12:00:22',11366147,5580,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2016-12-04 12:00:23',11360148,6580,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2016-12-04 12:00:24',11360146,6680,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2016-12-04 12:00:25',10060367,6780,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2016-12-05 12:00:26',10060197,6880,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2016-12-05 12:00:27',10060187,6980,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2016-12-05 12:00:28',10060113,9780,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2016-12-05 12:00:29',10060123,9880,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2016-12-05 12:00:30',10060133,9980,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2016-12-06 12:00:31',10060143,10080,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2016-12-06 12:00:32',10060151,11080,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2016-12-06 12:00:33',10060161,12080,' El sueldo fue depositado en el banco Violoncello');
INSERT INTO pagoSueldo (pagoFecha, cuentaNro, pagoMonto, pagoDetalle) VALUE ('2016-12-06 12:00:34',10060177,18080,' El sueldo fue depositado en el banco Violoncello');



select Ultimo_pago, Monto, tNombre, cuentaNro 
from (select date(max(pagoFecha)) Ultimo_pago, pagomonto Monto, cuentaNro 
	  from pagoSueldo group by cuentaNro) f, (select tNombre,tMonto 
	  										  from tTipo order by tMonto desc) as tmonto 
where f.Monto > tmonto group by cuentaNro;
/*			
+-------------+-------+----------+-----------+
| Ultimo_pago | Monto | tNombre  | cuentaNro |
+-------------+-------+----------+-----------+
| 2017-01-05  |  9780 | PLATEADA |  10060113 |
| 2017-01-05  |  9880 | PLATEADA |  10060123 |
| 2017-01-05  |  9980 | PLATEADA |  10060133 |
| 2017-01-06  | 10080 | DORADA   |  10060143 |
| 2017-01-06  | 11080 | DORADA   |  10060151 |
| 2017-01-06  | 12080 | DORADA   |  10060161 |
| 2017-01-06  | 18080 | DORADA   |  10060177 |
| 2017-01-05  |  6980 | PLATEADA |  10060187 |
| 2017-01-05  |  6880 | PLATEADA |  10060197 |
| 2017-01-04  |  6780 | PLATEADA |  10060367 |
| 2017-01-04  |  6680 | PLATEADA |  11360146 |
| 2017-01-04  |  6580 | PLATEADA |  11360148 |
| 2017-01-03  |  5580 | PLATEADA |  11366147 |
| 2017-01-03  |  5480 | PLATEADA |  11760312 |
| 2017-01-03  |  5380 | PLATEADA |  13260145 |
| 2017-01-03  |  5280 | PLATEADA |  14560021 |
| 2017-01-03  |  5180 | PLATEADA |  14560034 |
| 2017-01-03  |  5080 | PLATEADA |  14560045 |
| 2017-01-03  |  4980 | LATA     |  14560046 |
| 2017-01-03  |  4880 | LATA     |  14560121 |
| 2017-01-03  |  4780 | LATA     |  14560145 |
| 2017-01-03  |  4680 | LATA     |  14560147 |
| 2017-01-03  |  4580 | LATA     |  14560148 |
| 2017-01-03  |  4480 | LATA     |  14560312 |
+-------------+-------+----------+-----------+
*/

select Ultimo_pago, Monto, tNombre 'Tarjeta Tipo' ,qw.cuentaNro 'Nro de cuenta',p_dni DNI 
		,persona.p_nombre Nombre ,persona.p_apellido Apellido 
from  ( select Ultimo_pago, Monto, tNombre, cuentaNro 
   		from (select date(max(pagoFecha)) Ultimo_pago, pagomonto Monto, cuentaNro 
   			  from pagoSueldo group by cuentaNro) f, (select tNombre,tMonto 
   			  										  from tTipo order by tMonto desc) as tmonto 
   		where f.Monto > tmonto group by cuentaNro) qw, persona 
where persona.cuentaNro= qw.cuentaNro;

/*
+-------------+-------+--------------+---------------+----------+-----------------+------------+
| Ultimo_pago | Monto | Tarjeta Tipo | Nro de cuenta | DNI      | Nombre          | Apellido   |
+-------------+-------+--------------+---------------+----------+-----------------+------------+
| 2017-01-05  |  9780 | PLATEADA     |      10060113 | 11977399 | CLAUDIA         | ROMBOLA    |
| 2017-01-05  |  9880 | PLATEADA     |      10060123 | 12618318 | MARIA           | MACHADO    |
| 2017-01-05  |  9980 | PLATEADA     |      10060133 | 32422341 | NATALIA         | NUÑES      |
| 2017-01-06  | 10080 | DORADA       |      10060143 | 16347407 | MARCOS          | PINEDA     |
| 2017-01-06  | 11080 | DORADA       |      10060151 | 18213672 | LORENA          | MACHIONE   |
| 2017-01-06  | 12080 | DORADA       |      10060161 | 28672342 | ANGELA          | SILVERA    |
| 2017-01-06  | 18080 | DORADA       |      10060177 | 14537245 | MARIA INES      | FILIPELLI  |
| 2017-01-05  |  6980 | PLATEADA     |      10060187 | 26825931 | ANDREA          | LEDESMA    |
| 2017-01-05  |  6880 | PLATEADA     |      10060197 | 19756329 | SILVANA         | CEVERI     |
| 2017-01-04  |  6780 | PLATEADA     |      10060367 | 29707604 | LILIANA         | PENNIMPEDE |
| 2017-01-04  |  6680 | PLATEADA     |      11360146 | 22421332 | VANESA          | CANDIDO    |
| 2017-01-04  |  6580 | PLATEADA     |      11360148 | 34000456 | MARIELA         | CIFRE      |
| 2017-01-03  |  5580 | PLATEADA     |      11366147 | 26890341 | ELIDA           | FERRARA    |
| 2017-01-03  |  5480 | PLATEADA     |      11760312 | 19093574 | JUANA           | FORTE      |
| 2017-01-03  |  5380 | PLATEADA     |      13260145 | 23405341 | SILVIO          | RUGGIA     |
| 2017-01-03  |  5280 | PLATEADA     |      14560021 | 31440674 | MARIO EUGENIO   | ADAD       |
| 2017-01-03  |  5180 | PLATEADA     |      14560034 | 33029102 | MARIA EUGENIA   | SILVA      |
| 2017-01-03  |  5080 | PLATEADA     |      14560045 | 23678334 | SUSANA ANGELICA | FERNANDZ   |
| 2017-01-03  |  4980 | LATA         |      14560046 | 33566425 | MARIO           | GIMENEZ    |
| 2017-01-03  |  4880 | LATA         |      14560121 | 30123123 | ANDRES          | NUME       |
| 2017-01-03  |  4780 | LATA         |      14560145 | 32422341 | CAROLINA        | GUTIERREZ  |
| 2017-01-03  |  4680 | LATA         |      14560147 |  4579012 | SERGIO          | GIMENEZ    |
| 2017-01-03  |  4580 | LATA         |      14560148 | 28099456 | JUAQUINA        | VERA       |
| 2017-01-03  |  4480 | LATA         |      14560312 | 21308674 | MARTIN DARIO    | CONDE      |
+-------------+-------+--------------+---------------+----------+-----------------+------------+
*/

select * from persona 
where cuentaNro not in (select cuentaNro cNro 
 					    from pagoSueldo group by  cuentaNro) ;

/* sin  pago de sueldo registrado
+-----------+----------+------------+----------+---------------+----------+-----------+------+-------+----------+--------+
| cuentaNro | p_nombre | p_apellido | p_dni    | p_fnacimiento | sexo     | codPostal | piso | depto | calle    | numero |
+-----------+----------+------------+----------+---------------+----------+-----------+------+-------+----------+--------+
|  10060153 | JULIAN   | CORONEL    | 27579350 | 1979-06-08    | FEMENINO |      1820 |    1 | B     | VUCETICH |    450 |
+-----------+----------+------------+----------+---------------+----------+-----------+------+-------+----------+--------+
*/



