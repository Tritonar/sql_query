### Consultas en mysql.



1) Conductores que no tienen infracciones.

```sql
select * from  licencia  where not EXISTS 
(select licNro from infraccion where licencia.licNro=infraccion.licNro group by licNro );
```

| licNro   | apellido | nombre   | tipoLicencia | puntos | dni      |
|--|--|--|--|---|--|
| 10677563 | PEREZ    | MARIA    | Part         |    100 | 10677563 |
| 18564474 | AMARILLA | FERNANDO | Tran         |    100 | 18564474 |
| 21564444 | PEREZ    | JUAN     | Part         |    100 | 21564444 |


2) Mayor cantidad de infracciones
```sql
select a.licNro, a.apellido, a.nombre, a.dni, tp. Multas  
from licencia a,(select licNro, count(*) Multas
	 from ((select idDominio, licNro, tipoI from infraccion where licNro is not null) 
 			union all (select ty.idDominio, auto.dni licnro, ty.tipoI   
 			from (select * from infraccion where licNro is null) ty, auto 
 			where ty.idDominio= auto.idDominio    ) ) as rc, tipoInf rg 
 	 where rg.tipoI = rc.tipoI group by licNro limit 1 ) tp 
where tp.licNro= a.licNro;
```
```sql
select infraccion.licNro,nombre,apellido, count(infraccion.licNro) multas 
from infraccion, licencia where  infraccion.licNro!=0  and infraccion.licNro=licencia.licNro 
group by infraccion.licNro 
having multas=(select fg.DER 
			   from (select licNro, count(*) DER from infraccion 
			   	group by licNro order by DER desc limit 1) fg);

```

| licNro   | apellido | nombre | dni      | Multas |
|--|--|--|---|--|
| 11233445 | PALURDI  | JUAN   | 11233445 |      4 |

Por cantidad de infracciones de menor a mayor
```sql
select a.licNro, a.apellido, a.nombre, a.dni, tp. Multas  
from licencia a,(select licNro, count(*) Multas 
	from (  (select idDominio, licNro, tipoI from infraccion where licNro is not null) 
		    union all 
		    (select ty.idDominio, auto.dni licnro, ty.tipoI   
			 from (select * from infraccion where licNro is null) ty, auto 
			 where ty.idDominio= auto.idDominio    ) )
	as rc, tipoInf rg 
	where rg.tipoI = rc.tipoI group by licNro ) tp 
where tp.licNro= a.licNro order by tp.Multas;

```

| licNro   | apellido  | nombre | dni      | Multas |
|--|--|--|--|--|
| 23545333 | PALURDI   | HECTOR | 23545333 |      1 |
| 17111111 | ARANGUNDE | MARIAM | 17111111 |      3 |
| 11233445 | PALURDI   | JUAN   | 11233445 |      4 |

4) Actualice la tabla licencia restando los puntos
```sql
select a.licNro, a.apellido, a.nombre, a.dni, tp. Puntos  from licencia a,
	(select licNro, sum(rg.infPuntos) Puntos 
	 from ((select idDominio, licNro, tipoI from infraccion where licNro is not null) 
			union all (select ty.idDominio, auto.dni licnro, ty.tipoI   
						from (select * from infraccion where licNro is null) ty, auto 
						where ty.idDominio= auto.idDominio    ) ) as rc, tipoInf rg 
	 where rg.tipoI = rc.tipoI group by licNro desc ) tp
where tp.licNro= a.licNro;
```

| licNro   | apellido  | nombre | dni      | Puntos |
|---|---|---|---|---|
| 11233445 | PALURDI   | JUAN   | 11233445 |     80 |
| 17111111 | ARANGUNDE | MARIAM | 17111111 |     60 |
| 23545333 | PALURDI   | HECTOR | 23545333 |     30 |

4) Actualice la tabla licencia restando los puntos
```sql
select a.licNro, a.apellido, a.nombre, a.dni, (a.puntos -tp. Puntos) 'Puntos Tot' 
from licencia a,(select licNro, sum(rg.infPuntos) Puntos 
			     from ((select idDominio, licNro, tipoI from infraccion where licNro is not null) 
						union all (select ty.idDominio, auto.dni licnro, ty.tipoI   
								   from (select * from infraccion where licNro is null) ty, auto 
				 				   where ty.idDominio= auto.idDominio    ) ) as rc, tipoInf rg
				 where rg.tipoI = rc.tipoI group by licNro desc ) tp 
where tp.licNro= a.licNro; 
```


| licNro   | apellido  | nombre | dni      | Puntos Tot |
|--|--|---|---|---|
| 11233445 | PALURDI   | JUAN   | 11233445 |         20 |
| 17111111 | ARANGUNDE | MARIAM | 17111111 |         40 |
| 23545333 | PALURDI   | HECTOR | 23545333 |         70 |




El dominio con mayor cantidad de infracciones
```sql
select idDominio, count(*) Multas from infraccion group by idDominio order by Multas desc limit 1 ;
```

| idDominio | Multas |
|--|--|
| ABB122    |      5 |


```sql
select licNro , count(*) as cantidad 
from infraccion
where licNro>0
group by licNro
having count(*)=(select  count(licNro) from infraccion 
				  group by licNro order by count(licNro) desc limit 1)
;
```

Order by count(*) desc;


| licNro   | cantidad |
|---|---|
| 17111111 |        3 |


#### Ejercicio del banco.

```sql
select Ultimo_pago, Monto, tNombre, cuentaNro 
from (select date(max(pagoFecha)) Ultimo_pago, pagomonto Monto, cuentaNro 
        from pagoSueldo group by cuentaNro) f, 
        (select tNombre,tMonto from tTipo order by tMonto desc) as tmonto
             where f.Monto > tmonto group by cuentaNro;
```

| Ultimo_pago | Monto | tNombre  | cuentaNro |
|---|---|--|--|
| 2017-01-05  |  9780 | PLATEADA |  10060113 |
| 2017-01-05  |  9880 | PLATEADA |  10060123 |
| 2017-01-05  |  9980 | PLATEADA |  10060133 |
| 2017-01-06  | 10080 | DORADA   |  10060143 |
| 2017-01-06  | 11080 | DORADA   |  10060151 |
| 2017-01-06  | 12080 | DORADA   |  10060161 |
| 2017-01-06  | 18080 | DORADA   |  10060177 |
| 2017-01-05  |  6980 | PLATEADA |  10060187 |
| 2017-01-05  |  6880 | PLATEADA |  10060197 |
| 2017-01-04  |  6780 | PLATEADA |  10060367 |
| 2017-01-04  |  6680 | PLATEADA |  11360146 |
| 2017-01-04  |  6580 | PLATEADA |  11360148 |
| 2017-01-03  |  5580 | PLATEADA |  11366147 |
| 2017-01-03  |  5480 | PLATEADA |  11760312 |
| 2017-01-03  |  5380 | PLATEADA |  13260145 |
| 2017-01-03  |  5280 | PLATEADA |  14560021 |
| 2017-01-03  |  5180 | PLATEADA |  14560034 |
| 2017-01-03  |  5080 | PLATEADA |  14560045 |
| 2017-01-03  |  4980 | LATA     |  14560046 |
| 2017-01-03  |  4880 | LATA     |  14560121 |
| 2017-01-03  |  4780 | LATA     |  14560145 |
| 2017-01-03  |  4680 | LATA     |  14560147 |
| 2017-01-03  |  4580 | LATA     |  14560148 |
| 2017-01-03  |  4480 | LATA     |  14560312 |
**Sin  pago de sueldo registrado**
```sql
select * from persona 
where cuentaNro not in (select cuentaNro cNro 
 					    from pagoSueldo group by  cuentaNro) ;
                         ```
```
cuentaNro|p_nombre|p_apellido|p_dni|p_fnacimiento|sexo|codPostal | piso|depto|calle| numero |
--|--|--|--|--|--|--|--|--|--|--
10060153 | JULIAN   | CORONEL    | 27579350 | 1979-06-08    | FEMENINO |      1820 |    1 | B     | VUCETICH |    450 |
----

#### Clinica

```sql
SELECT  COUNT(DISTINCT cexterna.numHistoria) 
from cexterna where numHistoria not in (select fInterna.numHistoria from fInterna 
										where DATE(ffecha) BETWEEN '2016-01-01' and '2016-12-31' ) 
					and exfecha BETWEEN '2016-01-01' and '2016-12-31';
```

```sql
(SELECT DISTINCT cexterna.numHistoria from cexterna 
 where date(exfecha) BETWEEN '2015-01-01'and '2016-12-31') 
UNION (select fInterna.numHistoria from fInterna 
	   where DATE(ffecha) BETWEEN '2015-01-01' and '2016-12-31' 
	         and fInterna.numHistoria not in (select cexterna.numHistoria from cexterna 
	         	                              where date(cexterna.exfecha) BETWEEN '2015-01-01' and '2016-12-31'))
ORDER BY numHistoria;
```

#### Cantidad de pacientes atendidos en 2016 de ext. e int sin repetir
```sql
select count(*) 'Cantidad de pacientes atendidos en 2016 de ext. e int' 
from (select numHistoria from fInterna where 2016 = year(ffecha) 
	  union select numHistoria from cexterna where 2016=year(exfecha)  ) c;
```
| Cantidad de pacientes atendidos en 2016 de ext. e int |
|:---:|
|                                                     8 |


#### Cantidad de estudios totales enviados a GranLab en el 2016 
```sql
select  sum(val) 'Cantidad de estudios 2016' 
from (select codEstudio , count(*) val from fInterna 
	  where year(ffecha)=year(curdate()) and codEstudio is not null group by codEstudio 
	  union all 
	  select codEstudio , count(*) from cexterna 
	  where year(exfecha)=year(curdate()) and codEstudio is not null group by codEstudio) cv;
```

| Cantidad de estudios 2016 |
|---------------------------|
|                        17 |


#### Pacientes con mas de X consultas en un año particular
```sql
select  numHistoria 'Hist. Clinica', numCon Consultas 
from (select numHistoria, count(*) numCon 
	  from cexterna where 2017=year(exfecha) group by numHistoria) a 
where numCon >=2;
```
| Hist. Clinica | Consultas |
|:--:|:--:|
|      14560034 |         4 |
|      14560045 |         2 |
|      14560121 |         2 |

### Paciente internados, medico que ordeno Int.

```sql
select l2.fecha, l2.numHistoria 'Historia clinica', 
	   paciente.p_nombre 'Nombre', paciente.p_apellido 'Apellido' ,
	   l2.nMed 'Num. Med', l2.m_nombre 'Nombre medico' , l2.m_apellido 'Apellido' 
from (select fecha, numHistoria , l1.nMed , m_nombre , m_apellido 
	  from (select date (ffecha) 'fecha' , numHistoria  , nMed  
	  	    from fInterna where fsalida=0) l1 , medico 
	  where l1.nMed = medico.nMed ) l2 ,paciente  
where paciente.numHistoria =l2.numHistoria ;
```

| fecha      | Historia clinica | Nombre  | Apellido | Num. Med | Nombre medico | Apellido |
|--|--|--|---|--|--|--|
| 2017-09-24 |         10060133 | NATALIA | NUÑES    |     6619 | DIEGO GUSTAVO | ABDALA   |
| 2017-10-08 |         10060123 | MARIA   | MACHADO  |     4786 | ELVIRA ROSA   | ZJARIA   |
| 2017-10-14 |         10060143 | MARCOS  | PINEDA   |     6619 | DIEGO GUSTAVO | ABDALA   |
| 2017-10-21 |         10060161 | ANGELA  | SILVERA  |     4786 | ELVIRA ROSA   | ZJARIA   |

```sql
select * from cexterna;
```

| exfecha             | codEsp | detalleDiagnostico           | codMed | nMed | numHistoria | codPatologia | codEstudio |
|--|--|--|--|--|---|---|--|
| 2012-02-12 10:24:32 |     12 | Larengitis                   |   1011 | 6619 |    14560121 |        97002 |       NULL |
| 2012-08-12 13:15:23 |     12 | Gripe                        |   1022 | 6619 |    14560034 |        97005 |       NULL |
| 2012-12-29 10:13:11 |     12 | Intoxicacion                 |   1011 | 6619 |    14560045 |        98002 |       NULL |
| 2013-11-26 13:40:54 |     12 | Neumonia                     |   1018 | 6619 |    11366147 |        97006 |       NULL |
| 2014-04-08 15:03:57 |     13 | Otitis                       |   1022 | 3599 |    11360148 |        97004 |       NULL |
| 2014-04-18 11:22:15 |     12 | Gripe                        |   1011 | 6619 |    14560148 |        97005 |       NULL |
| 2015-04-18 12:42:15 |     12 | Gripe                        |   1022 | 6619 |    11760312 |        97005 |       NULL |
| 2015-06-14 12:22:08 |     14 | Fractura                     |   1014 | 4786 |    14560312 |        99002 |       NULL |
| 2015-08-12 11:45:23 |     12 | Gripe                        |   1011 | 6619 |    10060197 |        97005 |       NULL |
| 2016-01-01 01:04:54 |     12 | Vias respiratorias afectadas |   1011 | 6619 |    14560312 |        97006 |       NULL |
| 2016-04-08 15:03:57 |     13 | Otitis                       |   1022 | 3599 |    14560045 |        97004 |       NULL |
| 2016-04-16 10:11:23 |     15 | Tratamiento de conductos     |   1014 | 4786 |    14560145 |        97002 |       NULL |
| 2016-05-09 13:45:23 |     12 | Tratamiento de conductos     |   1014 | 9677 |    14560148 |        97004 |       NULL |
| 2016-06-14 11:12:08 |     14 | Fractura                     |   1011 | 4786 |    13260145 |        99002 |       NULL |
| 2016-12-29 11:23:11 |     12 | Intoxicacion                 |   1014 | 6619 |    10060367 |        98002 |       NULL |
| 2017-01-14 11:12:08 |     14 | Fractura                     |   1011 | 4786 |    13260145 |        99002 |       1011 |
| 2017-02-18 11:22:15 |     12 | Gripe                        |   1011 | 6619 |    14560148 |        97005 |       1012 |
| 2017-03-18 12:42:15 |     12 | Gripe                        |   1022 | 6619 |    11760312 |        97005 |       1013 |
| 2017-03-29 11:23:11 |     12 | Intoxicacion                 |   1014 | 6619 |    10060177 |        98002 |       1010 |
| 2017-04-29 10:13:11 |     12 | Intoxicacion                 |   1011 | 6619 |    14560045 |        98002 |       1011 |
| 2017-05-08 15:03:57 |     13 | Otitis                       |   1022 | 3599 |    14560045 |        97004 |       1012 |
| 2017-05-14 12:22:08 |     14 | Fractura                     |   1014 | 4786 |    14560312 |        99002 |       1013 |
| 2017-06-12 10:24:32 |     12 | Larengitis                   |   1011 | 6619 |    14560121 |        97002 |       1010 |
| 2017-06-12 11:45:23 |     12 | Gripe                        |   1011 | 6619 |    10060197 |        97005 |       1010 |
| 2017-06-17 12:24:00 |     14 | Tratamiento de conductos     |   1022 | 7475 |    14560121 |        97006 |       NULL |
| 2017-06-26 13:40:54 |     12 | Neumonia                     |   1018 | 6619 |    11366147 |        97006 |       1013 |
| 2017-08-12 13:15:23 |     12 | Gripe                        |   1022 | 6619 |    14560034 |        97005 |       1012 |
| 2017-08-16 12:45:23 |     12 | Tratamiento de conductos     |   1018 | 6876 |    14560147 |        98005 |       NULL |
| 2017-10-16 23:50:46 |     15 | Tratamiento de conductos     |   1011 |  948 |    14560034 |        98002 |       NULL |
| 2017-10-17 00:19:04 |     15 | Tratamiento de conductos     |   1011 |  948 |    14560034 |        98002 |       NULL |
| 2017-10-17 00:22:42 |     15 | Tratamiento de conductos     |   1011 |  948 |    14560034 |        98002 |       NULL |
| 2017-10-17 01:13:04 |     15 | Tratamiento de conductos     |   1014 | 4786 |    14560145 |        97002 |       NULL |
```sql
select * from fInterna;
```

| ffecha|nMed|numHistoria|codEsp|fdetalleDiagnostico|codPatologia|codEstudio | codMed | fsalida             |
|--|--|--|--|--|--|--|--|--|
| 2014-06-14 06:45:18 | 6619 |    10060133 |     12 | Farengitis          |        97002 |       NULL |   1011 | 2014-06-21 12:26:08 |
| 2015-06-14 07:26:03 | 6619 |    11366147 |     12 | Neumonia            |        97006 |       1011 |   1011 | 2015-06-21 21:12:08 |
| 2015-06-14 08:24:08 | 4786 |    14560312 |     14 | FRACTURA EXPUESTA   |        99002 |       1013 |   1011 | 2015-06-21 22:22:08 |
| 2015-06-14 09:23:28 | 6619 |    10060367 |     12 | Farengitis          |        97002 |       NULL |   1011 | 2015-06-21 13:23:08 |
| 2015-12-12 11:22:06 | 4786 |    10060197 |     14 | FRACTURA EXPUESTA   |        99002 |       1013 |   1011 | 2015-12-23 21:32:08 |
| 2016-06-14 09:25:04 | 6619 |    14560045 |     12 | Neumonia            |        97006 |       1011 |   1011 | 2016-06-21 12:23:08 |
| 2016-06-14 12:21:06 | 6619 |    14560148 |     12 | Neumonia            |        97006 |       1011 |   1011 | 2016-06-21 23:56:08 |
| 2016-06-14 18:13:18 | 6619 |    10060123 |     12 | Farengitis          |        97002 |       NULL |   1011 | 2016-06-21 15:26:08 |
| 2016-06-14 19:14:04 | 6619 |    13260145 |     12 | Farengitis          |        97002 |       NULL |   1011 | 2016-06-21 16:42:08 |
| 2016-10-24 04:44:03 | 4786 |    10060177 |     14 | FRACTURA EXPUESTA   |        99002 |       1013 |   1011 | 2016-10-26 06:52:08 |
| 2017-06-14 04:28:03 | 6619 |    14560147 |     12 | Neumonia            |        97006 |       1011 |   1011 | 2017-06-21 18:52:08 |
| 2017-06-14 16:04:05 | 6619 |    10060151 |     12 | Farengitis          |        97002 |       NULL |   1011 | 2017-06-21 17:26:08 |
| 2017-08-14 08:13:08 | 4786 |    10060161 |     14 | FRACTURA EXPUESTA   |        99002 |       1013 |   1011 | 2017-08-21 07:42:08 |
| 2017-09-24 07:26:03 | 6619 |    10060133 |     12 | Neumonia            |        97006 |       1011 |   1011 | 0000-00-00 00:00:00 |
| 2017-10-08 11:22:06 | 4786 |    10060123 |     14 | FRACTURA EXPUESTA   |        99002 |       1013 |   1011 | 0000-00-00 00:00:00 |
| 2017-10-14 09:25:04 | 6619 |    10060143 |     12 | Neumonia            |        97006 |       1011 |   1011 | 0000-00-00 00:00:00 |
| 2017-10-21 04:44:03 | 4786 |    10060161 |     14 | FRACTURA EXPUESTA   |        99002 |       1013 |   1011 | 0000-00-00 00:00:00 |

*/
select * from medico;
/*

| nMed | m_nombre         | m_apellido      |
|--|--|--|
|  948 | NELLY ELBA       | ZENTENO         |
| 1738 | RICARDO OCTAVIO  | ZELAYA          |
| 2048 | MIGUEL ANGEL     | AGUILERA        |
| 3599 | LUIS ORLANDO     | ABDALA          |
| 4704 | LUIS ROBERTO     | AGUDO SARACHAGA |
| 4786 | ELVIRA ROSA      | ZJARIA          |
| 6187 | CLAUDIA GRABIELA | ADAD            |
| 6619 | DIEGO GUSTAVO    | ABDALA          |
| 6876 | ERIKA ELIZABETH  | ZALAYA          |
| 7475 | MARIA EUGENIA    | ABALOS          |
| 9677 | NICOLAS          | ZELARAYAN       |
```sql
select * from paciente;
```

| numHistoria | p_nombre        | p_apellido | p_dni    | p_fnacimiento | sexo      | codPostal | piso | depto | calle           | numero |
|--|--|--|---|--|--|--|--|--|---|---|
|    10060113 | CLAUDIA         | ROMBOLA    | 11977399 | 2056-12-20    | MASCULINO |      1870 |    3 | D     | CRAMER          |   1931 |
|    10060123 | MARIA           | MACHADO    | 12618318 | 2057-01-12    | MASCULINO |      1873 |    2 | A     | ZARATE          |    225 |
|    10060133 | NATALIA         | NUÑES      | 32422341 | 1971-06-03    | FEMENINO  |      1872 |    1 | A     | ALEM            |   2232 |
|    10060143 | MARCOS          | PINEDA     | 16347407 | 2063-06-04    | MASCULINO |      1875 |    1 | A     | PELEGRINI       |    319 |
|    10060151 | LORENA          | MACHIONE   | 18213672 | 1975-07-20    | MASCULINO |      1872 |    1 | A     | CASTELLI        |    170 |
|    10060153 | JULIAN          | CORONEL    | 27579350 | 1979-06-08    | FEMENINO  |      1820 |    1 | B     | VUCETICH        |    450 |
|    10060161 | ANGELA          | SILVERA    | 28672342 | 1982-12-23    | FEMENINO  |      1871 |    1 | B     | LOMAZ DE ZAMORA |    757 |
|    10060177 | MARIA INES      | FILIPELLI  | 14537245 | 1976-03-12    | MASCULINO |      1873 |    2 | A     | CORONEL DORREGO |    578 |
|    10060187 | ANDREA          | LEDESMA    | 26825931 | 1978-10-08    | FEMENINO  |      1872 |    1 | A     | PRINGLES        |   2236 |
|    10060197 | SILVANA         | CEVERI     | 19756329 | 1973-03-19    | MASCULINO |      1871 |    3 | D     | ALSINA          |    430 |
|    10060367 | LILIANA         | PENNIMPEDE | 29707604 | 1982-09-13    | MASCULINO |      1874 |    1 | A     | DOLORES         |    378 |
|    11360146 | VANESA          | CANDIDO    | 22421332 | 1978-10-12    | MASCULINO |      1823 |    2 | A     | VIAMONTE        |   1027 |
|    11360148 | MARIELA         | CIFRE      | 34000456 | 1985-09-23    | FEMENINO  |      1874 |    1 | B     | ROCA            |    748 |
|    11366147 | ELIDA           | FERRARA    | 26890341 | 1976-02-25    | MASCULINO |      1873 |    1 | A     | SAN MARTIN      |    450 |
|    11760312 | JUANA           | FORTE      | 19093574 | 1976-05-12    | MASCULINO |      1872 |    1 | A     | Av. ALSINA      |    368 |
|    13260145 | SILVIO          | RUGGIA     | 23405341 | 1989-07-09    | FEMENINO  |      1822 |    1 | A     | LIBERTAD        |    613 |
|    14560021 | MARIO EUGENIO   | ADAD       | 31440674 | 1982-04-12    | MASCULINO |      1869 |    6 | Y     | Av. BELGRANO    |   6891 |
|    14560034 | MARIA EUGENIA   | SILVA      | 33029102 | 0000-00-00    | FEMENINO  |      1870 |    3 | D     | AV. MITRE       |   2346 |
|    14560045 | SUSANA ANGELICA | FERNANDZ   | 23678334 | 1978-10-12    | FEMENINO  |      1872 |    2 | H     | GUEMES          |    236 |
|    14560046 | MARIO           | GIMENEZ    | 33566425 | 1987-06-03    | MASCULINO |      1875 |    7 | J     | MARTIN FIERRO   |    547 |
|    14560121 | ANDRES          | NUME       | 30123123 | 1988-11-10    | MASCULINO |      1870 |    3 | D     | Av. BELGRANO    |   1291 |
|    14560145 | CAROLINA        | GUTIERREZ  | 32422341 | 1981-07-09    | FEMENINO  |      1872 |    1 | A     | PRINGLES        |   2236 |
|    14560147 | SERGIO          | GIMENEZ    |  4579012 | 1971-04-20    | MASCULINO |      1870 |    1 | A     | CEBALLOS        |   1457 |
|    14560148 | JUAQUINA        | VERA       | 28099456 | 1984-09-23    | FEMENINO  |      1870 |    1 | B     | GUEMES          |    890 |
|    14560312 | MARTIN DARIO    | CONDE      | 21308674 | 1984-03-02    | MASCULINO |      1870 |    1 | A     | Av. BELGRANO    |    601 |

```sql
select * from espmedico;
```
| nMed | codEsp |
|------|--------|
| 7475 |     11 |
| 3599 |     13 |
| 6619 |     12 |
| 4786 |     14 |
|  948 |     15 |
| 1738 |     16 |
| 6876 |     17 |
| 9677 |     18 |
| 6187 |     19 |
| 4704 |     20 |
| 2048 |     21 |